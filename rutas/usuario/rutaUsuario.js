const express=require("express");
const router=express.Router();
const conexion=require('../../conexion/conexion');

router.get('/usuario',async (req,res)=>{
    await conexion.query("select * from usuario",(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })

 router.get('/usuario/:id',(req,res)=>{
  const {id}=req.params;
  conexion.query("select * from usuario where id=?",[id],(err,rows,fields)=>{
    if(!err){
      res.json(rows);
    }else{
      res.json(err);
    }
  })
})

router.post('/usuario', async (req,res)=>{
    
  const {nombre,apellido,telefono,celular,estado,correo,contraseña}=req.body;
  await conexion.query("insert into usuarios set nombre=?,apellido=?,telefono=?,celular=?,estado=?,correo=?,contraseña=?",[nombre,apellido,telefono,celular,estado,correo,contraseña],(err,rows,fields)=>{
     if(!err){
       res.json(rows);
     }else{
        res.json(err);
     }
  })
})

router.put('/usuario/:id',(req,res)=>{
  const {id}=req.params;
  console.info(req.params);
  const {nombre,apellido,telefono,celular,estado,correo,contraseña}=req.body[0];
  console.log(req.body);
   conexion.query("UPDATE usuario SET nombre=?,apellido=?,telefono=?,celular=?,estado=?,correo=?,contraseña=? WHERE id=?",[nombre,apellido,telefono,celular,estado,correo,contraseña,id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
        console.log(rows);
      }else{
        res.json(err);
      }

  })
})

router.delete('/usuario/:id',(req,res)=>{
  const {id}=req.params;
   conexion.query("delete from usuario where id=?",[id],(err,rows,fields)=>{
    if(!err){
      res.json(rows);
    }else{
      res.json(err);
    }
  })
})


module.exports=router;