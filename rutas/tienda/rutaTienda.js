const express=require("express");
const router=express.Router();
const conexion=require('../../conexion/conexion');

router.get('/tienda',async (req,res)=>{
    await conexion.query("select * from tienda",(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
})

router.get('/tienda/:id',async (req,res)=>{
    const {id}=req.params;
    await conexion.query("select * from tienda where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
})

router.post('/tienda',async(req,res)=>{
    const{idUsuario,nombre,direccion,tipoTienda,horaAbierto,horaCerrado,popularidad,urlLogo}=req.body;
    await conexion.query("INSERT INTO tienda set idUsuario=?,nombre=?,direccion=?,tipoTienda=?,horaAbierto=?,horaCerrado=?,popularidad=?,urlLogo=?",[idUsuario,nombre,direccion,tipoTienda,horaAbierto,horaCerrado,popularidad,urlLogo],(err,rows,fields)=>{
        if(!err){
            res.json(rows);
        }else{
            res.json(err);
        }
    })
})


router.put('/tienda/:id',async(req,res)=>{
    const {id}=req.params;
    const{idUsuario,nombre,direccion,tipoTienda,horaAbierto,horaCerrado,popularidad,urlLogo}=req.body[0];
    await conexion.query("UPDATE tienda set idUsuario=?,nombre=?,direccion=?,tipoTienda=?,horaAbierto=?,horaCerrado=?,popularidad=?,urlLogo=? Where id=?",[idUsuario,nombre,direccion,tipoTienda,horaAbierto,horaCerrado,popularidad,urlLogo,id],(err,rows,fields)=>{
        if(!err){
            res.json(rows);
        }else{
            res.json(err);
        }
    })
})

router.delete('/tienda/:id',async (req,res)=>{
    const {id}=req.params;
    await conexion.query("delete from tienda where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
})

module.exports=router;