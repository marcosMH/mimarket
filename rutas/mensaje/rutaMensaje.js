const express=require("express");
const router=express.Router();
const conexion=require('../../conexion/conexion');

router.get('/mensaje',async (req,res)=>{
    await conexion.query("select * from mensaje",(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })
   router.get('/mensaje/:id',async (req,res)=>{
      const {id}=req.params;
      await conexion.query("select * from mensaje where id=?",[id],(err,rows,fields)=>{
        if(!err){
          res.json(rows);
        }else{
          res.json(err);
        }
      })
    })
  
    
    router.post('/mensaje', async (req,res)=>{   
      const {idUsuario,idRol,mensaje,estado,fechaCreacion}=req.body;
      await conexion.query("insert into mensaje set idUsuario=?,idRol=?,mensaje=?,estado=?,fechaCreacion=?",[idUsuario,idRol,mensaje,estado,fechaCreacion],(err,rows,fields)=>{
         if(!err){
           res.json(rows);
         }else{
            res.json(err);
         }
      })
   })
  
   router.put('/mensaje/:id', async (req,res)=>{
    const {id}=req.params;   
    const {idUsuario,idRol,mensaje,estado,fechaCreacion}=req.body[0];
    await conexion.query("set idUsuario=?,idRol=?,mensaje=?,estado=?,fechaCreacion=? where id=?",[idUsuario,idRol,mensaje,estado,fechaCreacion,id],(err,rows,fields)=>{
       if(!err){
         res.json(rows);
       }else{
          res.json(err);
       }
    })
  })
  
    router.delete('/mensaje/:id',async (req,res)=>{
      const {id}=req.params;
      await conexion.query("delete from mensaje where id=?",[id],(err,rows,fields)=>{
        if(!err){
          res.json(rows);
        }else{
          res.json(err);
        }
      })
    })
  
  
  module.exports=router;

module.exports=router;